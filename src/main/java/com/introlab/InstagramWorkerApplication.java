package com.introlab;

import com.introlab.instagram.scraper.UserDetailScraper;
import com.introlab.instagram.worker.Worker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class InstagramWorkerApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(InstagramWorkerApplication.class, args);
        Worker worker = context.getBean(Worker.class);
        worker.init();
        UserDetailScraper detailScraper = context.getBean(UserDetailScraper.class);
        detailScraper.initTokens(worker);
        new Thread(detailScraper::scrapeUserDetails).start();
    }
}
