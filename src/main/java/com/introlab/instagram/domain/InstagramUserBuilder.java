package com.introlab.instagram.domain;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class InstagramUserBuilder {
    private Long id;
    private String userName;
    private String fullName;
    private String description;
    private long amountOfFollowers;
    private long amountOfFollowing;
    private long amountOfPhotos;
    private Set<SearchQuery> tag;
    private double engagementRate;
    private String profilePicUrl;
    private String profilePicUrlHD;
    private String email;
    private boolean privateProfile = false;
    private boolean verifiedProfile = false;
    private String externalLink;
    private Date createdDate = new Date();
    private List<Media> medias = Collections.emptyList();
    private boolean countryBlock;
    private String link;
    private Long amountOfComments;
    private Long amountOfLikes;
    private Long workerId;

    public InstagramUserBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public InstagramUserBuilder setWorkerId(Long id) {
        this.workerId = id;
        return this;
    }

    public InstagramUserBuilder setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public InstagramUserBuilder setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public InstagramUserBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public InstagramUserBuilder setAmountOfFollowers(long amountOfFollowers) {
        this.amountOfFollowers = amountOfFollowers;
        return this;
    }

    public InstagramUserBuilder setAmountOfFollowing(long amountOfFollowing) {
        this.amountOfFollowing = amountOfFollowing;
        return this;
    }

    public InstagramUserBuilder setAmountOfPhotos(long amountOfPhotos) {
        this.amountOfPhotos = amountOfPhotos;
        return this;
    }


    public InstagramUserBuilder setTag(Set<SearchQuery> tag) {
        this.tag = tag;
        return this;
    }

    public InstagramUserBuilder setEngagementRate(double engagementRate) {
        this.engagementRate = engagementRate;
        return this;
    }

    public InstagramUserBuilder setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
        return this;
    }

    public InstagramUserBuilder setProfilePicUrlHD(String profilePicUrlHD) {
        this.profilePicUrlHD = profilePicUrlHD;
        return this;
    }

    public InstagramUserBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public InstagramUserBuilder setPrivateProfile(boolean privateProfile) {
        this.privateProfile = privateProfile;
        return this;
    }

    public InstagramUserBuilder setVerifiedProfile(boolean verifiedProfile) {
        this.verifiedProfile = verifiedProfile;
        return this;
    }

    public InstagramUserBuilder setExternalLink(String externalLink) {
        this.externalLink = externalLink;
        return this;
    }

    public InstagramUserBuilder setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public InstagramUserBuilder setMedias(List<Media> medias) {
        this.medias = medias;
        return this;
    }

    public InstagramUserBuilder setCountryBlock(boolean countryBlock) {
        this.countryBlock = countryBlock;
        return this;
    }

    public InstagramUserBuilder setAmmountOfLikes(Long ammountOfLikes) {
        this.amountOfLikes = ammountOfLikes;
        return this;
    }

    public InstagramUserBuilder setAmmountOfComments(Long ammountOfComments) {
        this.amountOfComments = ammountOfComments;
        return this;
    }

    public InstagramUserBuilder setLink(String link) {
        this.link = link;
        return this;
    }

    public InstagramUser createInstagramUser() {
        return new InstagramUser(id, userName, fullName, description, amountOfFollowers, amountOfFollowing, amountOfPhotos, tag, engagementRate, profilePicUrl, profilePicUrlHD, email, privateProfile, verifiedProfile, externalLink, createdDate, medias, countryBlock, link, amountOfComments, amountOfLikes,workerId);
    }
}