package com.introlab.instagram.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author vitalii.
 */
@Getter
@Setter
@Document
public class SearchQuery {
    @Id
    private String id;
    private String tags;
    private long postCount;
    private long userCount;
    private Long workerId;
    private boolean started;
    private boolean pause;
    private boolean finished;
    private String nextPageUrl;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date startDate;
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchQuery that = (SearchQuery) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
