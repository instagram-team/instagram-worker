package com.introlab.instagram.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vitalii.
 */
@Document
@Setter
@Getter
public class Droplet {

    @Id
    private Long id;
    private String ip;
    private String name;
    private String status;

}
