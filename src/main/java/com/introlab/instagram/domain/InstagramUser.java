package com.introlab.instagram.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author vitalii.
 */
@Getter
@Setter
@ToString
@Document(collection = "users")
public class InstagramUser {
    @Id
    private Long id;
    private String userName;
    private String fullName;
    private String firstName;
    private String lastName;
    private String description;
    private long amountOfFollowers;
    private long amountOfFollowing;
    private long amountOfPhotos;
    private Set<SearchQuery> tag;

    private double engagementRate;
    private String profilePicUrl;
    private String profilePicUrlHD;
    private String email;
    private boolean privateProfile;
    private boolean verifiedProfile;
    private String externalLink;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createdDate;
    private List<Media> medias;
    private boolean countryBlock;
    private String link;
    private Long amountOfComments;
    private Long amountOfLikes;
    private Long workerId;

    InstagramUser(Long id, String userName, String fullName, String description, long amountOfFollowers, long amountOfFollowing, long amountOfPhotos, Set<SearchQuery> tag, double engagementRate, String profilePicUrl, String profilePicUrlHD, String email, boolean privateProfile, boolean verifiedProfile, String externalLink, Date createdDate, List<Media> medias, boolean countryBlock, String link, Long amountOfComments, Long amountOfLikes, Long workerId) {
        this.id = id;
        this.userName = userName;
        this.fullName = fullName;
        this.description = description;
        this.amountOfFollowers = amountOfFollowers;
        this.amountOfFollowing = amountOfFollowing;
        this.amountOfPhotos = amountOfPhotos;
        this.tag = tag;
        this.engagementRate = engagementRate;
        this.profilePicUrl = profilePicUrl;
        this.profilePicUrlHD = profilePicUrlHD;
        this.email = email;
        this.privateProfile = privateProfile;
        this.verifiedProfile = verifiedProfile;
        this.externalLink = externalLink;
        this.createdDate = createdDate;
        this.medias = medias;
        this.countryBlock = countryBlock;
        this.link = link;
        this.amountOfComments = amountOfComments;
        this.amountOfLikes = amountOfLikes;
        this.workerId = workerId;
    }
}
