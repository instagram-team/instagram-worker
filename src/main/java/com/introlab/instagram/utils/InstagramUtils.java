package com.introlab.instagram.utils;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import static java.lang.Thread.sleep;

/**
 * @author vitalii.
 */
public class InstagramUtils {

    private InstagramUtils() {
    }

    public static String getMyIpV4() {
        String ip = null;
        List<String> ips = new ArrayList<>();
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                // filters out 127.0.0.1 and inactive interfaces
                if (iface.isLoopback() || !iface.isUp())
                    continue;

                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();

                    // *EDIT*
                    if (addr instanceof Inet6Address) continue;

                    ip = addr.getHostAddress();
                    System.out.println(ip);
                    ips.add(ip);
                }
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
        return ips.get(ips.size()-1);
    }

    public static void delay(int nanos) {
        try {
            sleep(nanos);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
