package com.introlab.instagram.scraper;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.introlab.instagram.domain.BaseUser;
import com.introlab.instagram.domain.SearchQuery;
import com.introlab.instagram.domain.Token;
import com.introlab.instagram.service.BaseUserService;
import com.introlab.instagram.service.SearchQueryService;
import com.introlab.instagram.worker.Worker;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

/**
 * @author Vitalii
 */
@Component
public class HashTagUserScraper {

    private Worker worker;
    private com.introlab.instagram.service.JSoupService JSoupService;
    private BaseUserService baseUserService;
    private SearchQueryService searchQueryService;

    private static final String END_POINT_TEMPLATE = "https://api.instagram.com/v1/tags/%s/media/recent";

    private List<String> tokens = new ArrayList<>();


    public HashTagUserScraper(Worker worker) {
        this.JSoupService = worker.getScrapingService().getJSoupService();
        this.baseUserService = worker.getScrapingService().getBaseUserService();
        this.searchQueryService = worker.getScrapingService().getSearchQueryService();
        this.worker = worker;
    }


    private void initTokens(Worker worker) {
        if (tokens.size() > 1) return;
        for (Token token : worker.getTokens()) {
            tokens.add("?access_token=" + token.getToken() + "&count=33");
        }
    }

    public void scrape(SearchQuery searchQuery) {

        initTokens(worker);
        if (searchQuery.isPause() || searchQuery.isFinished()) {
            return;
        }

        String scrapingUrl = searchQuery.getNextPageUrl() != null ? searchQuery.getNextPageUrl() : String.format(END_POINT_TEMPLATE, searchQuery.getTags()) + tokens.get(0);
        Document document = JSoupService.fetchForCrwaling(scrapingUrl);
        if (document.text().length() < 100) {
            worker.getSearchQueries().remove(searchQuery);
            return;
        }
        final JsonElement parse = new JsonParser().parse(document.text());

        Set<BaseUser> baseUsers = new HashSet<>();
        JsonArray data = getMediasAsJsonArray(parse);
        for (JsonElement datum : data) {
            baseUsers.add(parseBaseUser(searchQuery, datum));
        }
        String nextURL = getNextPageUrl(parse);
        baseUserService.save(baseUsers);
        searchQuery.setNextPageUrl(nextURL);

        searchQuery = updateDataBase(baseUsers.size(), searchQuery);
        if (isLastPage(searchQuery, nextURL)) return;
        int pageCount =1;
        while (searchQuery.getPostCount() <= worker.getMaxPostsCount()) {
            pageCount++;
            if (pageCount%100==0){
                searchQuery.setUserCount(baseUserService.countBySearchQuery(searchQuery).intValue());
            }
            if (searchQuery.isPause()) {
                worker.getSearchQueries().remove(searchQuery);
                return;
            }
            for (String token : tokens) {
                baseUsers = new HashSet<>();
                document = JSoupService.fetchForCrwaling(nextURL);
                JsonElement jsonElement = new JsonParser().parse(document.text());

                nextURL = composeNextPageUrl(token, jsonElement, searchQuery.getTags());
                JsonArray dataArray = getMediasAsJsonArray(jsonElement);
                for (JsonElement datum : dataArray) {
                    baseUsers.add(parseBaseUser(searchQuery, datum));
                }
                baseUserService.save(baseUsers);
                if (isLastPage(searchQuery, nextURL)) {
                    updateDataBase(baseUsers.size(), searchQuery);
                    return;
                }
                searchQuery = updateDataBase(baseUsers.size(), searchQuery);
                if (!Objects.equals(searchQuery.getWorkerId(), worker.getDroplet().getId())) {
                    return;
                }
                searchQuery.setNextPageUrl(nextURL);
                searchQueryService.save(searchQuery);
            }
        }
        if (searchQuery.getPostCount() > worker.getMaxPostsCount()) {
            searchQuery.setFinished(true);
            worker.getSearchQueries().remove(searchQuery);
        }
        searchQueryService.save(searchQuery);
    }

    private boolean isLastPage(SearchQuery searchQuery, String nextURL) {
        if (nextURL.isEmpty()) {
            searchQuery.setFinished(true);
            searchQueryService.save(searchQuery);
            worker.getSearchQueries().remove(searchQuery);
            return true;
        }
        return false;
    }

    private SearchQuery updateDataBase(int postCount, SearchQuery searchQuery) {
        searchQuery.setPostCount(searchQuery.getPostCount() + postCount);
//        searchQuery.setUserCount(baseUserService.countBySearchQuery(searchQuery).intValue());
        return searchQueryService.save(searchQuery);
    }

    private JsonArray getMediasAsJsonArray(JsonElement jsonElement) {
        return jsonElement.getAsJsonObject().getAsJsonArray("data");
    }

    private BaseUser parseBaseUser(SearchQuery query, JsonElement datum) {
        Long userId = getOwnerUserIdFromMedia(datum);
        BaseUser user = baseUserService.findBiId(userId);
        if (user != null) {
            user.getTag().add(query);
            baseUserService.save(user);
            return user;
        } else {
            BaseUser baseUser = new BaseUser();
            baseUser.setId(userId);
            baseUser.setUserName(getOwnerUserNameFromMedia(datum));
            baseUser.setTag(Collections.singleton(query));
            return baseUser;
        }
    }

    private Long getOwnerUserIdFromMedia(JsonElement datum) {
        return datum.getAsJsonObject().getAsJsonObject("user").getAsJsonPrimitive("id").getAsLong();
    }

    private String getOwnerUserNameFromMedia(JsonElement datum) {
        return datum.getAsJsonObject().getAsJsonObject("user").getAsJsonPrimitive("username").getAsString();
    }

    private String getNextPageUrl(JsonElement parse) {
        JsonObject pagination = parse.getAsJsonObject().getAsJsonObject("pagination");
        if (pagination != null && pagination.has("next_url")) {
            String next_url = pagination.getAsJsonPrimitive("next_url").getAsString();
            try {
                next_url = new String(next_url.getBytes("UTF-8"), "UTF-8");
                next_url = URLDecoder.decode(next_url, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return next_url;
        }
        return "";
    }

    private String composeNextPageUrl(String token, JsonElement jsonElement, String tag) {
        String nexPageId = getNexPageId(jsonElement);
        return nexPageId.equals("last") ? "" : String.format(END_POINT_TEMPLATE, tag) + token + "&max_tag_id=" + nexPageId;
    }

    private String getNexPageId(JsonElement jsonElement) {
        JsonObject pagination = jsonElement.getAsJsonObject().getAsJsonObject("pagination");
        return pagination.has("next_max_tag_id") ? pagination.getAsJsonPrimitive("next_max_tag_id").getAsString() : "last";
    }
}
