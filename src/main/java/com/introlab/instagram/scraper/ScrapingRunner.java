package com.introlab.instagram.scraper;

import com.introlab.instagram.domain.SearchQuery;
import com.introlab.instagram.observers.Observer;
import com.introlab.instagram.worker.Worker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author vitalii.
 */
@Component
public class ScrapingRunner implements Observer {

    private final HashTagUserScraper hashTagUserScraper;

    @Autowired
    public ScrapingRunner(Worker worker, HashTagUserScraper hashTagUserScraper) {
        this.hashTagUserScraper = hashTagUserScraper;
        worker.registerObserver(this);
    }

    @Override
    public void update(SearchQuery searchQuery) {
        //TODO: run scraper
        runInNewThread(searchQuery);
        System.out.println(searchQuery.getTags());
    }

    private void runInNewThread(SearchQuery searchQuery){
        new Thread(() -> hashTagUserScraper.scrape(searchQuery)).start();
    }
}
