package com.introlab.instagram.scraper;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.introlab.instagram.domain.*;
import com.introlab.instagram.service.JSoupService;
import com.introlab.instagram.service.ScrapingService;
import com.introlab.instagram.worker.Worker;
import com.vdurmont.emoji.EmojiParser;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrBuilder;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.lang.Thread.sleep;


/**
 * @author vitalii.
 */
@Component
public class UserDetailScraper {

    private final ScrapingService scrapingService;

    private final JSoupService jSoupService;

    private final Worker worker;

    private final String BASE_URL = "https://instagram.com/";
    private final String API_TEMPLATE = "https://api.instagram.com/v1/users/%s/media/recent/%s";

    @Autowired
    public UserDetailScraper(ScrapingService scrapingService, Worker worker) {
        this.scrapingService = scrapingService;
        this.jSoupService = scrapingService.getJSoupService();
        this.worker = worker;
    }

    private List<String> tokens = new ArrayList<>();

    public void initTokens(Worker worker) {
        if (tokens.size() > 1) return;
        for (Token token : worker.getTokens()) {
            tokens.add("?access_token=" + token.getToken() + "&count=33");
        }
    }

    public void scrapeUserDetails() {

        while (true) {

            ExecutorService executor = Executors.newFixedThreadPool(7);
            int i = 0;
            final List<InstagramUser> instagramUsers = new ArrayList<>();
            List<BaseUser> usersForScrape = scrapingService.getBaseUserService().getUsersForScrape();
            if (usersForScrape.size() < 1) {
                try {
                    sleep(10000);
                    continue;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            for (BaseUser baseUser : usersForScrape) {
                int finalI = i;

                executor.execute(() -> {

                    try {

                        sleep(1000);

                        InstagramUser user = scrape(baseUser, tokens.get(finalI));

                        if (!user.getEmail().isEmpty()){
                            instagramUsers.add(user);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                });
                if (i == tokens.size() - 1) {
                    i = 0;
                } else {
                    i++;
                }
            }

            executor.shutdown();

            try {
                executor.awaitTermination(3, TimeUnit.DAYS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                scrapingService.getInstagramUserService().save(instagramUsers);
            } catch (Throwable throwable) {
                try {
                    sleep(5000);
                    scrapingService.getInstagramUserService().save(instagramUsers);
                } catch (Throwable e) {
                    e.printStackTrace();
                    throwable.printStackTrace();
                }
            }
        }
    }

    public InstagramUser scrape(BaseUser baseUser, String token) {

        InstagramUser instagramUser = scrapeUserFromWeb(baseUser);
        if (instagramUser.getEmail().isEmpty()){
            return instagramUser;
        }
        Document document = jSoupService.fetch(composeApiMediasLink(String.valueOf(baseUser.getId()), token));
        if (!instagramUser.isPrivateProfile() && instagramUser.getMedias().size() != 1) {
            try {
                updateUserAfterApiCall(instagramUser, document);
            } catch (NullPointerException ignored) {
                System.out.println("NPE in API call");
            }
        }
        return instagramUser;
    }

    private void updateUserAfterApiCall(InstagramUser instagramUser, Document document) {
        if (document.html().contains("you cannot view this")) {
            System.out.println("not allowed " + instagramUser.getLink());
            return;
        }
        JsonArray jsonMedias = new JsonParser().parse(document.text()).getAsJsonObject().getAsJsonArray("data");

        List<Media> mediaList = new ArrayList<>();

        long totalAmountOfLikes = 0;
        long totalAmountOfComments = 0;

        if (jsonMedias == null) {
            return;
        }
        for (JsonElement jsonMedia : jsonMedias) {
            JsonObject mediaObject = jsonMedia.getAsJsonObject();
            String link = mediaObject.getAsJsonPrimitive("link").getAsString();
            Long timestamp = Long.parseLong(mediaObject.getAsJsonPrimitive("created_time").getAsString());
            String location = !mediaObject.get("location").isJsonNull() && mediaObject.getAsJsonObject("location").has("name") ? mediaObject.getAsJsonObject("location").getAsJsonPrimitive("name").getAsString() : "";
            Double latitude = !mediaObject.get("location").isJsonNull() && mediaObject.getAsJsonObject("location").has("name") ? mediaObject.getAsJsonObject("location").getAsJsonPrimitive("latitude").getAsDouble() : 0;
            Double longitude = !mediaObject.get("location").isJsonNull() && mediaObject.getAsJsonObject("location").has("name") ? mediaObject.getAsJsonObject("location").getAsJsonPrimitive("longitude").getAsDouble() : 0;
            totalAmountOfLikes += mediaObject.getAsJsonObject("likes").getAsJsonPrimitive("count").getAsLong();
            totalAmountOfComments += mediaObject.getAsJsonObject("comments").getAsJsonPrimitive("count").getAsLong();
            StrBuilder tags = new StrBuilder();
            for (JsonElement tag : mediaObject.getAsJsonArray("tags")) {
                tags.append("#").append(tag.getAsJsonPrimitive().getAsString());
            }
            String id = mediaObject.getAsJsonPrimitive("id").getAsString();
            mediaList.add(new Media(id, location, link, tags.toString(), latitude, longitude, timestamp));
        }

        if (mediaList.size() > 0) {
            instagramUser.setAmountOfComments(totalAmountOfComments / mediaList.size());
            instagramUser.setAmountOfLikes(totalAmountOfLikes / mediaList.size());
        }
        try {
            instagramUser.setEngagementRate((double) instagramUser.getAmountOfLikes() / (double) instagramUser.getAmountOfFollowers());
        } catch (NullPointerException ignored) {
        }

        instagramUser.setMedias(mediaList);
    }

    private InstagramUser scrapeUserFromWeb(BaseUser baseUser) {

        String webLink = composeWebLink(baseUser.getUserName());
        Document document = jSoupService.fetch(webLink);
        if (document.text().contains("The link you followed may be broken, or the page may have been removed")) {
            return new InstagramUserBuilder().setId(baseUser.getId()).setFullName("The link you followed may be broken, or the page may have been removed").createInstagramUser();
        }

        for (Element script : document.select("script")) {
            if (script.html().contains("sharedData")) {
                InstagramUser user = parseUserFromWebPage(baseUser, script);
                if (user.getEmail().isEmpty()){
                    return user;
                }
                scrapingService.splitName(user);
                if (document.text().contains("No posts yet")) {
                    user.getMedias().add(new Media());
                }
                user.setId(baseUser.getId());
                return user;
            }
        }


        return new InstagramUserBuilder().createInstagramUser();
    }

    private InstagramUser parseUserFromWebPage(BaseUser baseUser, Element script) {
        String json = StringUtils.substringBeforeLast(StringUtils.substringAfter(script.unwrap().outerHtml(), "_sharedData = "), ";");
        JsonElement jsonElement = new JsonParser().parse(json);
        JsonObject userJsonObject = jsonElement.getAsJsonObject().getAsJsonObject("entry_data").getAsJsonArray("ProfilePage").get(0).getAsJsonObject().getAsJsonObject("user");
        String biography = userJsonObject.get("biography").isJsonNull() ? "" : userJsonObject.getAsJsonPrimitive("biography").getAsString();
        boolean countryBlock = userJsonObject.getAsJsonPrimitive("country_block").getAsBoolean();
        String externalUrl = userJsonObject.get("external_url").isJsonNull() ? "" : userJsonObject.getAsJsonPrimitive("external_url").getAsString();
        long amountOfFollowers = userJsonObject.getAsJsonObject("followed_by").getAsJsonPrimitive("count").getAsLong();
        long amountOfFollowing = userJsonObject.getAsJsonObject("follows").getAsJsonPrimitive("count").getAsLong();
        String fullName = userJsonObject.get("full_name").isJsonNull() ? "" : userJsonObject.getAsJsonPrimitive("full_name").getAsString();
        boolean isPrivate = userJsonObject.getAsJsonPrimitive("is_private").getAsBoolean();
        boolean isVerified = userJsonObject.getAsJsonPrimitive("is_verified").getAsBoolean();
        String profilePicUrl = userJsonObject.getAsJsonPrimitive("profile_pic_url").getAsString();
        String profilePicUrlHd = userJsonObject.getAsJsonPrimitive("profile_pic_url_hd").getAsString();
        int amountOfMedia = userJsonObject.getAsJsonObject("media").getAsJsonPrimitive("count").getAsInt();
        biography = decodeEmoji(biography);
        fullName = decodeEmoji(fullName);
        return new InstagramUserBuilder()
                .setUserName(baseUser.getUserName())
                .setId(baseUser.getId())
                .setLink(composeWebLink(baseUser.getUserName()))
                .setDescription(biography)
                .setCountryBlock(countryBlock)
                .setExternalLink(externalUrl)
                .setAmountOfFollowers(amountOfFollowers)
                .setAmountOfFollowing(amountOfFollowing)
                .setFullName(fullName)
                .setEmail(findEmails(biography))
                .setPrivateProfile(isPrivate)
                .setVerifiedProfile(isVerified)
                .setProfilePicUrl(profilePicUrl)
                .setProfilePicUrlHD(profilePicUrlHd)
                .setAmountOfPhotos(amountOfMedia)
                .setTag(baseUser.getTag())
                .setWorkerId(worker.getDroplet().getId())
                .createInstagramUser();
    }

    private String decodeEmoji(String biography) {
        if (biography != null) {
            biography = EmojiParser.parseToAliases(biography);
            biography = EmojiParser.parseToUnicode(biography);
        }
        return biography;
    }

    private String composeWebLink(String userName) {
        return BASE_URL + userName;
    }

    private String composeApiMediasLink(String userId, String token) {
        return String.format(API_TEMPLATE, userId, token);
    }

    private String findEmails(String text) {
        String emailString;
        Pattern pattern = Pattern.compile("\\w[\\w-._]+@[\\w-._]+[\\w]{2,4}");
        Matcher matcher = pattern.matcher(text);
        List<String> emails = new ArrayList<>();
        while (matcher.find()) {
            emails.add(matcher.group());
        }
        emailString = emails.stream().collect(Collectors.joining(","));
        return emailString;
    }
}
