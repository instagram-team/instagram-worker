package com.introlab.instagram.worker;

import com.introlab.instagram.domain.BaseUser;
import com.introlab.instagram.domain.Droplet;
import com.introlab.instagram.domain.SearchQuery;
import com.introlab.instagram.domain.Token;
import com.introlab.instagram.observers.Observable;
import com.introlab.instagram.observers.Observer;
import com.introlab.instagram.service.ScrapingService;
import com.introlab.instagram.service.SearchQueryService;
import com.introlab.instagram.service.WorkerService;
import com.introlab.instagram.utils.InstagramUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.introlab.instagram.utils.InstagramUtils.delay;
import static java.lang.Thread.sleep;

/**
 * @author vitalii.
 */
@Component
@Getter
@Setter
public class Worker implements Observable {

    private List<Observer> observers = new ArrayList<>();

    private long maxPostsCount = 1500000;
    private int maxTokensCount = 10;
    private int maxTagsCount = 5;
    private String workerIp;
    private Droplet droplet;
    private List<SearchQuery> searchQueries = new ArrayList<>();
    private List<Token> tokens = new ArrayList<>();

    @Autowired
    private WorkerService workerService;

    @Autowired
    private SearchQueryService searchQueryService;

    @Autowired
    private ScrapingService scrapingService;

    public void init() {
        try {
            sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(workerIp);
        String myIpV4 = InstagramUtils.getMyIpV4();
        workerIp = myIpV4;
        System.out.println("Worker ip:" + myIpV4);
        droplet = workerService.getDropletByIp(myIpV4);
//        droplet=new Droplet();
//        droplet.setId(124L);
        initTokens();
        initSearchQueries();
        new Thread(this::runNewTagsListener).start();

    }

    private void initSearchQueries() {
        for (int i = 0; i < maxTagsCount; i++) {
            SearchQuery searchQuery = searchQueryService.getAvailableSearchQuery(droplet.getId());
            if (searchQuery != null) {
                searchQueries.add(searchQuery);
                notifyObservers(searchQuery);
            }
        }
    }

    private void initTokens() {
        tokens.addAll(workerService.getTokensForDroplet(droplet.getId()));
    }

    private void runNewTagsListener() {
        while (true) {
            if (maxTagsCount > searchQueries.size()) {
                SearchQuery searchQuery = searchQueryService.getAvailableSearchQuery(droplet.getId());
                if (searchQuery != null) {
                    searchQueries.add(searchQuery);
                    notifyObservers(searchQuery);
                }
                System.out.println("Checked for new tag!!!");
            }
            delay(10000);

        }
    }


    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers(SearchQuery searchQuery) {
        for (Observer observer : observers)
            observer.update(searchQuery);
    }
}
