package com.introlab.instagram.repository;

import com.introlab.instagram.domain.InstagramUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * @author vitalii.
 */
public interface InstagramUserRepository extends MongoRepository<InstagramUser, Long> {

    @Query(value = "{tag._id : ?0}", count = true)
    Long countByTag(String query);

    @Query("{tag._id : ?0}")
    List<InstagramUser> findByTag(String taId);
    @Query(value = "{$and: [{ 'email' : { $regex: ?0 }}, {'tag._id' : ?1}]}",count = true)
    Long countDistinctByEmailContainsAndTag(String dog, String query);
    @Query(value = "{ 'email' : { $regex: ?0 }}",count = true)
    Long countDistinctByEmailContains(String dog);
    Long countAllById();
}
