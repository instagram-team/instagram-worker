package com.introlab.instagram.repository;

import com.introlab.instagram.domain.Droplet;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author vitalii.
 */
public interface DropletRepository extends MongoRepository<Droplet, Long> {

    Droplet findByIp(String ip);
}
