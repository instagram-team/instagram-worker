package com.introlab.instagram.repository;

import com.introlab.instagram.domain.SearchQuery;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author vitalii.
 */
public interface SearchQueryRepository extends MongoRepository<SearchQuery, String> {

    SearchQuery findFirstByStarted(boolean started);
}
