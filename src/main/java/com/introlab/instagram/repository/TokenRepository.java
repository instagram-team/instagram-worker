package com.introlab.instagram.repository;

import com.introlab.instagram.domain.Token;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author vitalii.
 */
public interface TokenRepository extends MongoRepository<Token, Long> {

    Token findFirstByInUse(boolean inUse);
    List<Token> findByDropletId(Long dropletId);
}
