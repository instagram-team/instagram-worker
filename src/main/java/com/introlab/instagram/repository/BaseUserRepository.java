package com.introlab.instagram.repository;

import com.introlab.instagram.domain.BaseUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * @author vitalii.
 */
public interface BaseUserRepository extends MongoRepository<BaseUser, Long> {
    //    Long countBySearchQuery(String queryId);
    @Query(value = "{tag._id : ?0}", count = true)
    Long countByTag(String query);

    List<BaseUser> findAllByScraped(boolean scraped);
}
