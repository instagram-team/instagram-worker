package com.introlab.instagram.observers;

import com.introlab.instagram.domain.SearchQuery;

/**
 * @author vitalii.
 */
public interface Observable {
    void registerObserver(Observer o);
    void removeObserver(Observer o);
    void notifyObservers(SearchQuery searchQuery);
}