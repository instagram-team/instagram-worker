package com.introlab.instagram.observers;

import com.introlab.instagram.domain.SearchQuery;

/**
 * @author vitalii.
 */
public interface Observer {
    void update(SearchQuery searchQuery);
}
