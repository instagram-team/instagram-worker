package com.introlab.instagram.service;

import com.introlab.instagram.domain.SearchQuery;
import com.introlab.instagram.repository.SearchQueryRepository;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author vitalii.
 */
@Service
@Setter
public class SearchQueryService {

    private final SearchQueryRepository searchQueryRepository;

    @Autowired
    public SearchQueryService(SearchQueryRepository searchQueryRepository) {
        this.searchQueryRepository = searchQueryRepository;
    }

    public SearchQuery save(SearchQuery searchQuery) {
        SearchQuery queryFromDb = findById(searchQuery.getId());
        return queryFromDb != null && queryFromDb.isPause() ? queryFromDb : searchQueryRepository.save(searchQuery);
    }

    public SearchQuery findById(String queryId) {
        return searchQueryRepository.findOne(queryId);
    }

    public List<SearchQuery> findAll() {
        return searchQueryRepository.findAll();
    }

    public SearchQuery getAvailableSearchQuery(Long workerId) {
        SearchQuery searchQuery = searchQueryRepository.findFirstByStarted(false);
        if (searchQuery != null) {
            searchQuery.setWorkerId(workerId);
            searchQuery.setStarted(true);
            searchQuery.setStartDate(new Date());
            System.out.println(searchQuery);
            return searchQueryRepository.save(searchQuery);
        }
        return null;
    }
}
