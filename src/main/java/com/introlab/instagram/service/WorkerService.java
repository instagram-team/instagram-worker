package com.introlab.instagram.service;

import com.introlab.instagram.domain.Droplet;
import com.introlab.instagram.domain.Token;
import com.introlab.instagram.repository.DropletRepository;
import com.introlab.instagram.repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author vitalii.
 */
@Service
public class WorkerService {

    private final DropletRepository dropletRepository;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    public WorkerService(DropletRepository dropletRepository) {
        this.dropletRepository = dropletRepository;
    }

    public Droplet getDropletByIp(String ip){
        return dropletRepository.findByIp(ip);
    }

    public Token getAvailableToken(Long dropletId){
        Token token = tokenRepository.findFirstByInUse(false);
        if (token!=null) {
            token.setInUse(true);
            token.setDropletId(dropletId);
            tokenRepository.save(token);
            return token;
        }
        return null;
    }

    public List<Token> getTokensForDroplet(Long dropletId){
        return  tokenRepository.findByDropletId(dropletId);
    }
}
