package com.introlab.instagram.service;

import com.introlab.instagram.domain.InstagramUser;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author vitalii.
 */
@Getter
@Service
public class ScrapingService {
    private final JSoupService JSoupService;

    private final BaseUserService baseUserService;

    private final InstagramUserService instagramUserService;

    private final SearchQueryService searchQueryService;

    @Autowired
    public ScrapingService(JSoupService JSoupService, BaseUserService baseUserService, InstagramUserService instagramUserService, SearchQueryService searchQueryService) {
        this.JSoupService = JSoupService;
        this.baseUserService = baseUserService;
        this.instagramUserService = instagramUserService;
        this.searchQueryService = searchQueryService;
    }

    public void splitName(InstagramUser instagramUser) {
        String cleanName = instagramUser.getFullName().replaceAll("[^AĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻaąbcćdeęfghijklłmnńoóprsśtuwyzźżA-Za-zQVXÅÄÖqvxåäö\\s]+", "");
        if (cleanName != null && !cleanName.isEmpty()) {

            String[] split = cleanName.split("\\s+");

            if (split.length > 1) {
                instagramUser.setFirstName(split[0]);
                instagramUser.setLastName(split[1]);
            } else if (split.length != 0) {
                instagramUser.setFirstName(split[0]);
                instagramUser.setLastName("");
            }
        }
    }

}
