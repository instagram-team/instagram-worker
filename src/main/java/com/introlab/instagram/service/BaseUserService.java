package com.introlab.instagram.service;

import com.introlab.instagram.domain.BaseUser;
import com.introlab.instagram.domain.SearchQuery;
import com.introlab.instagram.repository.BaseUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * @author vitalii.
 */
@Service
@PropertySource("classpath:config.properties")
public class BaseUserService {

    private final BaseUserRepository userRepository;

    @Value("${server-end-point}")
    private String endPoint;

    @Autowired
    public BaseUserService(BaseUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void save(BaseUser instagramUser) {
        userRepository.save(instagramUser);
    }

    public void save(Set<BaseUser> instagramUsers) {
        userRepository.save(instagramUsers);
    }

    public Long countBySearchQuery(SearchQuery query) {
        return userRepository.countByTag(query.getId());
    }

    public BaseUser findBiId(Long id) {
        return userRepository.findOne(id);
    }

    public List<BaseUser> getForDetailScraping() {
        return userRepository.findAllByScraped(false);
    }

    public List<BaseUser> getUsersForScrape() {
        final String uri = endPoint+"/instagram/baseusers";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<BaseUser[]> result;
        try {
            result = restTemplate.getForEntity(uri, BaseUser[].class);
            System.out.println("Received new pack of "+ result.getBody().length + " users.");
            return Arrays.asList(result.getBody());
        }catch (Exception e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
