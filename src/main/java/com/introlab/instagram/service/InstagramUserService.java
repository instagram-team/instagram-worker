package com.introlab.instagram.service;

import com.introlab.instagram.domain.InstagramUser;
import com.introlab.instagram.domain.SearchQuery;
import com.introlab.instagram.repository.InstagramUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author vitalii.
 */
@Service
public class InstagramUserService {

    private final InstagramUserRepository userRepository;

    @Autowired
    public InstagramUserService(InstagramUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void save(InstagramUser instagramUser) {
        userRepository.save(instagramUser);
    }

    public void save(List<InstagramUser> instagramUsers) {
        userRepository.save(instagramUsers);
    }

    public Long countByQueryId(SearchQuery query) {
        return userRepository.countByTag(query.getId());
    }

    public Long count() {
        return userRepository.count();
    }

    public Long countDistinctByEmailExistsAndQueryId(SearchQuery query) {
        return userRepository.countDistinctByEmailContainsAndTag(".*@.*", query.getId());
    }

    public Long countDistinctByEmailExists() {
        return userRepository.countDistinctByEmailContains(".*@.*");
    }

    public List<InstagramUser> findByQuery(SearchQuery query) {
        return userRepository.findByTag(query.getId());
    }
}
